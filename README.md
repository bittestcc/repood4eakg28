# Ruby sample

A sample ruby microservice block for [kintohub](http://kintohub.com)


# First time setup
(use with `ruby 2.5`)

* run `gem install bundler`
* run `bundle install`

# IMPORTANT
  this example project is built using using docker file from this repo which
  may contain version of language different from the one you've set in Kintohub's UI

  If you want to use the version you've set in Kintohub's UI -> just remove the Dockerfile

# Run

* `bundle exec ruby app.rb` run on port 8000 (port can be set by setting `PORT` env variable)
